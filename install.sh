#!/bin/bash

mkdir snp_snip_tmp
cd snp_snip_tmp

git clone https://gitlab.com/capuchin-owl-alliance/snp_snip.git

cd snp_snip

chmod a+x ./snp_snip.rb
sudo cp ./snp_snip.rb /usr/bin/snp_snip

cd ../..
rm -rf ./snp_snip_tmp
