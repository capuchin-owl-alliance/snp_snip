#!/usr/bin/ruby

require 'net/http'
require 'optparse'

VERSION = '0.0.4'
DEDICATION = {
  to: 'Susana Restrepo',
  for: 'Like, everything, man'
}

class Search

  class << self
    def main!(alleles, genome_file)
      alleles.each do |allele|


        target_allele = SNPedia.new(allele)
        target_snps = target_allele.snp_variations.map do |pair|
          {
            rsid: pair.first,
            allele: pair.last
          }
        end

        target_snps.each do |snp|
          puts "Target allele correlated to SNP #{snp[:rsid]}(#{snp[:allele]})"
        end

        genome = Genome.new(genome_file).genome

        target_snps.each do |target_snp|
          genome.each do |genome_snp|
            if target_snp[:rsid] == genome_snp[:rsid]
              puts "Search genome exhibits #{genome_snp[:allele]} at #{genome_snp[:rsid]}"
              puts " -- target mutation is correlated with allele #{target_snp[:allele]}"
              puts " -- subject is #{compare(genome_snp[:allele], target_snp[:allele])} target mutation"
            end
          end
        end
      end
    end

    private

    def compare(genome_allele, target_allele)
      if genome_allele =~ /#{target_allele}{2}/
        return "HOMOZYGOUS FOR"
      elsif genome_allele =~ /#{target_allele}/
        return "HETEROZYGOUS FOR"
      else
        return "AGAINST"
      end
    end
  end
end

class Genome
  attr_reader :genome

  def initialize(file)
    raw_genome_file = File.read(file)
    genome_file = raw_genome_file.split(/\r\n/).reject { |line| line =~ /#/ }

    @genome = genome_file.map do |genome_line|
      raw = genome_line.split(/\t/)
      {
        rsid: raw.first,
        allele: raw.last
      }
    end
  end
end

class SNPedia
  attr_reader :page_content

  def initialize(allele)
    uri = URI("http://www.snpedia.com/index.php/#{allele}")

    @page_content = Net::HTTP.get(uri)
  end

  def snp_variations
    # Given a protein allele, digs up any linked SNP alleles
    snp_regex = /<a .*>(rs\d+)<\/a>\((\w)\)/

    page_content.scan(snp_regex)
  end
end

options = {
  genome_file: String.new,
  alleles: Array.new
}

OptionParser.new do |opts|
  opts.banner = "Usage: snp_snip[.rb] --genome GENOME_FILE --allele ALLELE_NAME [--allele ALLELE_NAME]"

  opts.on("-g", "--genome GENOME_FILE", "Locate the genome file to search") do |genome_file|
    options[:genome_file] = genome_file
  end

  opts.on("-a", "--allele ALLELE_NAME", "Name the target allele.",
    "NOTE: This argument can be included an arbitrary number of times",
    "allowing search for multiple alleles") do |allele|
    options[:alleles] << allele
  end

  opts.separator ""
  opts.separator "Common options:"

  opts.on_tail("-h", "--help", "Show this message") do
    puts opts
    exit
  end

  opts.on_tail("-v", "--version", "Show version") do
    puts "SNP Snip version #{VERSION}"
    exit
  end

  opts.on_tail "-d", "--dedication", "Show dedication" do
    puts "To: #{DEDICATION[:to]}"
    puts "For: #{DEDICATION[:for]}"
    exit
  end
end.parse!

Search.main!(options[:alleles], options[:genome_file])
